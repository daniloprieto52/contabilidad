import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminusersmasterComponent } from './adminusersmaster.component';

describe('AdminusersmasterComponent', () => {
  let component: AdminusersmasterComponent;
  let fixture: ComponentFixture<AdminusersmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminusersmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminusersmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
