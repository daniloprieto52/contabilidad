import { TestBed } from '@angular/core/testing';

import { DataConsumerService } from './data-consumer.service';

describe('DataConsumerService', () => {
  let service: DataConsumerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataConsumerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
