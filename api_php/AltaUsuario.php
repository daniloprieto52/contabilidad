<?php 
  header('Access-Control-Allow-Origin: *'); 
  header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
  
  $hash = md5( rand(0,1000) ); // GENERA UN HASH de 32 CARACTERES ALEATORIO
  
  $json = file_get_contents('php://input'); // RECIBE EL JSON DE ANGULAR
 
  $params = json_decode($json); // DECODIFICA EL JSON Y LO GUARADA EN LA VARIABLE
  
  require("conexion.php"); // IMPORTA EL ARCHIVO CON LA CONEXION A LA DB

  $conexion = conexion(); // CREA LA CONEXION
  
  // REALIZA LA QUERY A LA DB
  mysqli_query($conexion, "INSERT INTO usuarios(nombre, telefono, email, pass, consumer_key, consumer_secret, type, hash, provincia, ciudad, direccion) VALUES
                  ('$params->nombre','$params->telefono', '$params->email', '$params->pass', '$params->consumer_key', '$params->consumer_secret','$params->type','$hash', '$params->provincia', '$params->ciudad', '$params->direccion')");    
  
  class Result {}

  // GENERA LOS DATOS DE RESPUESTA
  $response = new Result();
  $response->resultado = 'OK';
  $response->mensaje = 'SE REGISTRO EXITOSAMENTE EL USUARIO';

  header('Content-Type: application/json');

  echo json_encode($response); // MUESTRA EL JSON GENERADO
 
 
// ENVIO EMAIL DE VALIDACION 
$to      = $params->email;
$subject = 'carrito.com.ar | Verificacion'; 
$message = '
 
Gracias por Registrarte!
Tu cuenta fue creada, solo debes activarla desde el siguiente link:
 
------------------------
Usuario: '.$params->email.'
Contraseña: '.$params->passDecrypted.'
------------------------
Haz click en el link para activar tu cuenta:
https://www.carrito.com.ar/wp/api_php/verify.php?email='.$params->email.'&hash='.$hash.'
 
'; // Our message above including the link
                     
$headers = 'From:verificacion@carrito.com.ar' . "\r\n"; // Set from headers
mail($to, $subject, $message, $headers); // Send our email
?>