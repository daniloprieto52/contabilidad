<?php
// SDK de Mercado Pago
require __DIR__ .  '/vendor/autoload.php';


// Agrega credenciales
MercadoPago\SDK::setAccessToken('TEST-8959818651776105-102312-9f0964ef8985723d3094443eeeccfaa2-662478826');

// Crea un objeto de preferencia
$preference = new MercadoPago\Preference();


$id_order= $_GET['id_order'];
$name= $_GET['name'];
$quantity=$_GET['quantity'];
$unit_price=$_GET['unit_price'];

//reenvia a thanks si el pago se aprobo

$preference->back_urls = array(
    "success" => "https://carrito.com.ar/front/#/thanks/$id_order/completed"); 

// Crea un ítem en la preferencia

$datos=array();
//for($x=0;$x<10; $x++){
    $item = new MercadoPago\Item();
    $item->title = $name;
    $item->quantity = $quantity;
    $item->unit_price = $unit_price;
    // $datos[]=$item;
//}

// $preference->items = $datos;
$preference->items = array($item);
$preference->save();

?>

<!DOCTYPE html>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;700&display=swap" rel="stylesheet">
    <title>Pagar</title>
</head>
<style type="text/css">
    body{
        font-family: 'Lato', sans-serif;
    }
    @media(min-width:360px) and (max-width:374px){
        #stepOne{
            margin-top:15%;
            display:block;
            font-family: Lato;
            font-weight: bold;
            width:302px;
            height:241px;
            margin-left:auto;
            margin-right:auto;
            box-sizing: content-box;
            // border: 0.5px solid #0076B9;
            border-radius: 13px;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        }
        .header{
            background-color: #0076B9;
            color:white;
            text-align:center;
            border-radius: 13px 13px 0px 0px;
            padding-top: 3%;
            padding-bottom: 2%;
        }
        .header h1{
            text-align: center;
            color:white;
            font-size: 20px;
            font-weight: bold;
        }
        .content{
            box-sizing: content-box;
            border-radius: 0px 0px 13px 13px;
            background: #EAEDED;
            padding-left: 5%;
            padding-right:5%;
        }
        .content span{
            color:#61296C;
        }
        .info-shop{
            display:inline-flex;
            width: 100%;
        }
        .info-shop img{
            border-radius:100%;
            margin-top:2%;
            height: 50px;
        }
        
        .info-shop span{
            padding: 2%;
            margin-right: 2%;
            margin-top:2.5%;
            width: 40%;
            text-align: left;
            color:#61296C;
            font-size:12px;
        }
        
        .info-product{
            display:inline-flex;
            width:100%;
        }
        .content-image-product{
            display:inline-flex;
            width:50%;
            border-radius: 13px;
            overflow: hidden;
            margin-right: 5%;
            
        }
        .content-image-product img{
            width: 100%;
        }
        .content-info-product{
            width: 100%;
            text-align:center;
        }
        .content-info-product h2{
            color:#61296C;
            font-size: 16px;
            font-weight: bold;
        }
        .content-info-product span{
            color:#61296C;
            font-size: 16px;
            font-family: Lato;
            font-style: normal;
        }
        .content-quantity{
            display:inline-flex;
            width: 100%;
        }
        .content-quantity label{
            color:#0076B9;
            font-size: 12px;
            width:100%;
        }
        .content-quantity input{
            width:40%;
            height:20px;
        }
        select[name="shipping"]{
            background: #0076B9;
            color: white;
            width: 100%;
        }
        #ok{
            margin-top:5%;
            margin-bottom: 2%;
            background: #0076B9;
        }
        
        form{
            color:#61296C;
            font-weight: normal;
            min-height:320px;
        }
        form span{
            font-size: 13px;
        }
        
        form input{
            width: 100%;
            margin-bottom: 5px;
            padding-left:5%;
            border:0px solid transparent;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-radius: 3px;
        }
    }
</style>
<body>

    <div id="stepOne">
        <div class="header">
            <div style="width:100%;text-align:center">
                <img src="https://carrito.com.ar/icons/mercadopago-icon.svg"/>
            </div>
        </div>
        <div class="content">

            <div>
                <div class="info-product">
                    <div class="content-info-product">
                        <h2><?php echo $item->title ?></h2>
                        <br>
                        <div class="content-quantity">
                            <label for="cantidad">Cantidad: <?php echo $item->quantity ?></label>
                        </div>
             
                        <br>
                        <span>TOTAL: $<?php echo $item->unit_price ?></span>
                        <br>
                    </div>
                </div>
                <br>
        
                <br>
                <div style="width:100%;text-align:center">
                    <form action="https://carrito.com.ar/front" method="POST">
                        <script
                            src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                            data-preference-id="<?php echo $preference->id; ?>">
                        </script>
                    </form> 
                </div>
                
            </div>
        </div> 
    </div>

</body>
</html>


<!--

curl -X POST \
-H "Content-Type: application/json" \
-H 'Authorization: Bearer TEST-3107990581487844-072017-0f510f9750ddb9546d28fd2f07e9ef5f-59431580' \
"https://api.mercadopago.com/users/test_user" \
-d '{"site_id":"MLA"}'

vendedor = {"id":662478826,"nickname":"TETE5196319","password":"qatest4651","site_status":"active","email":"test_user_93028378@testuser.com"}

comprador = {"id":662478906,"nickname":"TESTEHWOHEUA","password":"qatest1599","site_status":"active","email":"test_user_43275741@testuser.com"}
-->